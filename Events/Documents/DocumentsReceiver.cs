﻿using System;
using System.IO;
using System.Timers;
using System.Collections.Generic;
using System.Linq;

namespace Documents
{
	public class DocumentsReceiver: IDisposable
	{
		public event Action DocumentsReady;
		public event Action TimedOut;

		private FileSystemWatcher _fw;
		private Timer _timer;
		private IEnumerable<string> _documentsToReceive;

		public void Start(string targetDirectory, int waitingInterval)
		{
			_documentsToReceive = new string[] { "Паспорт.jpg", "Заявление.txt", "Фото.jpg" };
			_fw = new FileSystemWatcher(targetDirectory);
			_fw.Created += OnDocumentAdded;
			_fw.Renamed += OnDocumentAdded;
			_fw.EnableRaisingEvents = true;
			_timer = new Timer(waitingInterval);
			_timer.Elapsed += OnTimerElapsed;
			_timer.Start();
		}

		private void OnDocumentAdded(object sender, FileSystemEventArgs args)
		{
			_documentsToReceive = _documentsToReceive.Where(d => d != args.Name);
			if (!_documentsToReceive.Any())
			{
				Unsubscribe();
				DocumentsReady?.Invoke();
			}
		}

		private void OnTimerElapsed(object sender, ElapsedEventArgs args)
		{
			Unsubscribe();
			TimedOut?.Invoke();
		}

		private void Unsubscribe()
		{
			_timer.Elapsed -= OnTimerElapsed;
			_fw.Created -= OnDocumentAdded;
			_fw.Renamed -= OnDocumentAdded;
		}

		public void Dispose()
		{
			_timer.Dispose();
			_fw.Dispose();
		}
	}
}
