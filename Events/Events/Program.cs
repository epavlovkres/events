﻿using Documents;
using System;

namespace Events
{
	class Program
	{
		static void Main(string[] args)
		{
			var dr = new DocumentsReceiver();
			dr.DocumentsReady += () => { Console.WriteLine("Documents ready!"); };
			dr.TimedOut += () => { Console.WriteLine("Timeout!"); };
			dr.Start("F:/testDir", 10000);
			Console.ReadLine();
		}
	}
}
